#!/bin/sh
set -ex

# Setup drupal site
/init-app.sh

# Run Nginx
exec nginx -g "daemon off;"